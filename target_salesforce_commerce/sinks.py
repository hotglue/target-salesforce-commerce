"""SalesforceCommerce target sink class, which handles writing streams."""
from hotglue_models_ecommerce.ecommerce import SalesOrder

from target_salesforce_commerce.client import SalesforceCommerceSink


class SalesOrdersSink(SalesforceCommerceSink):
    """SalesforceCommerce order target sink class."""

    endpoint = "baskets"
    unified_schema = SalesOrder
    name = SalesOrder.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record = self.validate_input(record)

        product_items = list(
            map(
                lambda line_item: {
                    "product_id": line_item["product_id"],
                    "quantity": line_item["quantity"],
                },
                record["line_items"],
            )
        )

        names = record.get("customer_name").split()
        first_name = names[0]
        last_name = " ".join(names[1:])

        billing_address = {}
        if record.get("billing_address"):
            billing_address = {
                "address1": record.get("billing_address")["line1"],
                "city": record.get("billing_address")["city"],
                "country_code": record.get("billing_address")["country"],
                "first_name": first_name,
                "full_name": record.get("customer_name"),
                "last_name": last_name,
                "phone": record.get("phone"),
                "postal_code": record.get("billing_address")["postal_code"],
                "state_code": record.get("billing_address")["state"],
            }

        shipping_address = {}
        if record.get("shipping_address"):
            shipping_address = {
                "address1": record.get("shipping_address")["line1"],
                "city": record.get("shipping_address")["city"],
                "country_code": record.get("shipping_address")["country"],
                "first_name": first_name,
                "full_name": record.get("customer_name"),
                "last_name": last_name,
                "phone": record.get("phone"),
                "postal_code": record.get("shipping_address")["postal_code"],
                "state_code": record.get("shipping_address")["state"],
            }

        mapping = {
            "agent_basket": True,
            "billing_address": billing_address,
            "shipments": [
                {
                    "shipping_address": shipping_address,
                    "shipping_method": {"id": "SHIP-001"},
                }
            ],
            "channel_type": "storefront",
            "payment_instruments": [
                {"payment_method_id": record.get("payment_method")}
            ],
            "product_items": product_items,
        }
        return self.validate_output(mapping)

    def upsert_record(self, record: dict, context: dict):
        endpoint = "/baskets"
        state_updates = dict()

        if record:
            try:
                basket_response = self.request_api(
                    "POST", endpoint=endpoint, request_data=record
                )

                basket_id = basket_response.json()["basket_id"]
                self.logger.info(f"Basked created with id: {basket_id}, {record}")

                order_response = self.request_api(
                    "POST",
                    endpoint="/orders",
                    request_data={
                        "basket_id": basket_id,
                    },
                )
                order_id = order_response.json()["order_token"]
                self.logger.info(
                    f"Order created with order_token: {order_id} for basket {basket_id}"
                )
            except:
                raise KeyError
            return order_id, True, state_updates
