"""SalesforceCommerce target class."""

from io import FileIO
from pathlib import Path, PurePath
from typing import Callable, Dict, List, Optional, Tuple, Type, Union
import copy
import time

from singer_sdk import typing as th
from singer_sdk.sinks import Sink
from singer_sdk.target_base import Target

import click

from singer_sdk.cli import common_options
from singer_sdk.helpers._classproperty import classproperty
from singer_sdk.mapper import PluginMapper
from singer_sdk.helpers._secrets import SecretString
from singer_sdk.helpers._util import read_json_file
from target_hotglue.target import TargetHotglue

from target_salesforce_commerce.sinks import SalesOrdersSink


class TargetSalesforceCommerce(TargetHotglue):
    SINK_TYPES = [SalesOrdersSink]
    MAX_PARALLELISM = 10
    name = "target-salesforce-commerce"


if __name__ == "__main__":
    TargetSalesforceCommerce.cli()
