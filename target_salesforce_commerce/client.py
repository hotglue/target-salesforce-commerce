from target_hotglue.client import HotglueSink
import requests
import base64


class SalesforceCommerceSink(HotglueSink):

    api_version = "v23_1"

    @property
    def base_url(self) -> str:
        domain = self.config.get("sf_domain", self.config.get("domain"))
        site_id = self.config["site_id"]
        base_url = f"https://{domain}.dx.commercecloud.salesforce.com/s/{site_id}/dw/shop/{self.api_version}"
        return base_url

    @property
    def http_headers(self):
        domain = self.config.get("sf_domain", self.config.get("domain"))
        client_id = self.config["client_id"]
        auth_str = f"{self.config['username']}:{self.config['password']}:{self.config['client_secret']}"
        auth_header = base64.b64encode(auth_str.encode("ascii")).decode("ascii")

        r = requests.post(
            f"https://{domain}.dx.commercecloud.salesforce.com/dw/oauth2/access_token?client_id={client_id}",
            headers={"Authorization": f"Basic {auth_header}"},
            data={
                "grant_type": "urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken"
            },
        )
        auth_payload = r.json()
        auth_credentials = {
            "Authorization": f"Bearer {auth_payload.get('access_token')}"
        }
        return auth_credentials

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()
